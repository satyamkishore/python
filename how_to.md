*  To Get 
  ~~~~
  import os
  import getpass
  import socket
  import time
  #print(os.environ)
  print(os.environ['PATH'])                            # Get Env var
  print("Current File Name : ",os.path.realpath(__file__)) #  get the path and name of the file that is currently executing
  print(getpass.getuser())                             # Get Current User
  print((socket.gethostname()))                        # Get PC name
  print(socket.gethostbyname(socket.gethostname()))    # Get local IP
  print(time.ctime(os.path.getmtime("counter.py")))    # Get Last modified date
  print(time.ctime(os.path.getctime("sample.py")))     # Get created on date
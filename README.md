# README #

This README is to keep track of Python Learning.

### What is this repository for? ###

For Machine learning hands-on using Python


https://machinelearningmastery.com/machine-learning-in-python-step-by-step/
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

###What did I learn today? ###

* ##### Check Python version in script
~~~~
import sys
print("Python version" ,sys.version)
print("Version info.", sys.version_info)
~~~~
* ##### How to display the current date and time.

~~~~
from datetime import datetime
print(datetime.now())
print(datetime.now().strftime('%Y/%m/%d %H:%M:%S'))
~~~~
* ##### To print the calendar of a given month and year.
~~~~
 from calendar import month
 y = int(input("Input the year : "))
 m = int(input("Input the month : "))
 print(month(y,m))
~~~~
* ##### To calculate number of days between two dates.
~~~~
from datetime import date
date1 =date(2014, 7, 2)
date2 =date(2014, 7, 11)
print((date2-date1).days)
~~~~
* ##### Calculate are of circle(πr2) 
~~~~
from math import pi
radius = float(input())
print(pi * radius **2)    
~~~~
* ##### To print the documents (syntax, description etc.) of Python built-in function(s).
`print(abs.__doc__)`

* **isinstance()** - Returns True if the specified object is of the specified type, otherwise False.

  _Note:_ If the type parameter is a tuple, this function will return True if the object is one of the types in the tuple.

  _Syntax_
~~~~
   isinstance(object, type)
   x = isinstance("Hello", (float, int, str, list, dict, tuple))
~~~~ 
* ##### How to call an external command in Python
  
~~~~
   from subprocess import call
   call(["ls", "-l"])
~~~~  
* ##### To determine profiling of Python programs.

   _Note:_ A profile is a set of statistics that describes how often and for how long various parts of the program executed. 
These statistics can be formatted into reports via the pstats module.
~~~~
  import cProfile
  def sum():
      print(1+2)
  cProfile.run('sum()')
~~~~
* ##### Print on the same line using end option. 
  `for i`` in 'abcde':
         print(i, end='')`
* ##### How to Sort a dictionary.

    Dictionaries are unordered data structures. Sorting a dictionary returns a list containing tuples of key-value pairs They are stored by the hash value of the key, and this allows for fast lookup.*
~~~~
   orders = {
    	'cappuccino': '54',
    	'latte': '56',
    	'espresso': '72',
    	'americano': '48',
    	'cortado': '41'
    }
    sort_orders = sorted(orders.items(), key=lambda x: x[1])
    
    for i in sort_orders:
    	print(i[0], i[1])
    #[print(key, value) for (key, value) in sorted(orders.items(), key=lambda x: x[1])]
~~~~
* **Enumerate()** This method adds counter to an iterable and returns it (the enumerate object).
  
  *Reference Example*: /src/test_dictionary.ipynb/31
~~~~
  grocery = ['bread', 'milk', 'butter']
  
  for item in enumerate(grocery):
    print(item)
  
  print('\n')
  for count, item in enumerate(grocery):
    print(count, item)
  
  print('\n')
  # changing default start value
  for count, item in enumerate(grocery, 100):
    print(count, item)
~~~~

* ##### Multi Line comment. 
  Python doesn't really have a syntax for this but it can be achieved this way as long as string 
  literals that are not assigned to a variable. Python will read the code but then ignore it. *Example*  
~~~~ 
  """
  This is a comment
  written in
  more than just one line
  """
~~~~
* ##### Use global keyword to create a variable with global scope
* ##### How to assign values to multiple variables in one line
~~~~
  x, y, z = "Orange", "Banana", "Cherry"
  x = y = z = "Orange"
~~~~
* ##### Built in data-types
~~~~
  Text Type:	str
  Numeric Types:	int, float, complex
  Sequence Types:	list["apple", "banana", "cherry"], tuple("apple", "banana", "cherry"), range 
  Mapping Type:	dict
  Set Types:	set{"apple", "banana", "cherry"}, frozenset
  Boolean Type:	bool
  Binary Types:	bytes, bytearray, memoryview
~~~~
* ##### Random number genereator:
~~~~ 
  import random
  print(random.randrange(1, 10))  
~~~~

## List 
*  A `list` is a collection of elements that can contain duplicate elements and has a defined order that generally does 
   not change unless explicitly made to do so. **stacks** and **queues** are both types of lists that provide specific 
   (often limited) behavior for adding and removing elements (stacks being LIFO, queues being FIFO). 
   Lists are practical representations of, well, lists of things. A string can be thought of as a list of characters, 
   as the order is important ("abc" != "bca") and duplicates in the content of the string are certainly permitted
   ("aaa" can exist and != "a").
   
*  `append()` - Appends an element to the end of the list 
*  `insert()` - Add an item at the specified index    
*  `remove()` - removes the specified item      `thislist.remove("banana")`
*  `pop()` - removes the specified index, (or the last item if index is not specified)      `thislist.pop(0)`
*  `del()` - removes the specified index, (or can also delete the list completely if index not specified)
    
    `del thislist[0]`
*  `clear` - empties the list     `thislist.clear()`
*   `copy` - Make a copy of a list or use the built-in method `list()`

    `mylist = thislist.copy()`     
    `mylist = list(thislist)`
*   `reverse` -   reverses the sorting order of the elements   `list.reverse()`
*   `sort` - sorts the list ascending by default `list.sort(reverse=True|False, key=myFunc)`
~~~~       
     cars = ['Ford', 'Mitsubishi', 'BMW', 'VW']
     cars.sort(reverse=True , key=lambda x : len(x))
~~~~
*   Concatenate List - 
    - Use + Sign Eg. - (list1 + list2) 
    - extend() - list1.extend(list2) To add list2 at the end of list1
    - append() - list1.append(list2)
## Tuple

*   A **tuple** is a collection which is ordered and unchangeable or immutable(Add , Remove item not possible). **`thistuple = ("apple", "banana", "cherry")`**
*   To change tuple values . -> Convert into list -> Change value -> convert back to tuple
~~~~
    y = list(thistuple)
    y[1] = "kiwi"
    thistuple = tuple(y)
~~~~
*   To create a tuple with one item , you need to add a comma after the item or Python will not recognize it as a tuple. **`thistuple = ("apple",)`**
*   `del()` - Delete the tuple completely 
*   Concatenate Tuple Using + Sign Eg. - (tuple1 + tuple2)
*   **`count() , index()`** - Two built-in methods that you can use on tuples. 

## Set

*   A **set** is a collection which is unordered (no gurantee on the order items will appear) , iterable and unindexed
    (cannot access items in a set by referring to an index or a key) . **`thisset = {"apple", "banana", "cherry"}`**
*   Loop through using `for` loop to access items in set .
*   Once a set is created, you cannot change its items, but you can add new items.
    - **`add()`** - To add one item to a set . `thisset.add("orange")`
    - **`update()`** - To add more than one item to a set . ` thisset.update(["orange", "mango", "grapes"])`
*   `remove()` or `discard()` - To remove an item in a set. 

      _Note:_ If the item to remove does not exist,` remove() `will raise an error but `discard()` will not.
*   `union()` returns a **new set** containing all items from both sets.
*   `update()` method that inserts all the items from one set into another `set1.update(set2)`
     
     _Note:-_ Both `union()` and `update()` will exclude any duplicate items.
     
*   `difference()` - Return a set that contains items which exist only in the **set1**, and not in both sets.`newset = set1.difference(set2)`
*   `difference_update()` - Remove the items from **set1** which exist in both sets `set1.difference_update(set2)`
*   `intersection()` - Return a set that contains the similarity between two or more sets.`newset = set.intersection(set1, set2 ... etc)`
*   `intersection_update()` - Remove the items from first set that is not present in both sets (or in all sets ). `set.intersection_update(set1, set2 ... etc)`
*   `isdisjoint()` - Returns True if none of the items are present in both sets, otherwise it returns False. `set1.isdisjoint(set2)`
*   `issubset()` - Returns True if all items in the set1 exists in the specified set2, otherwise it retuns False. `set1.issubset(set2)`
*   `issuperset()` - Returns True if all items in the specified set exists in the original set, otherwise it retuns False. `set1.issuperset(set2)`
*   `symmetric_difference()` - Returns a set that contains all items from both set, but not the items that are present in both sets. `newset = set.symmetric_difference(set)`
*   `symmetric_difference_update()` - Updates the set1 by removing items that are present in both sets, and inserting the other items from set2.    `set1.symmetric_difference_update(set2)`    

* ##### To check if specified key is present in a dictionary 

     *The `<=` & `>=` operator for sets tests for whether the set on the left/right is a subset of the other*
   
~~~~
   issubset(other)
   set <= other
   Test whether every element in the set is in other.
   
   set < other
   Test whether the set is a proper subset of other, that is, set <= other and set != other.
   
   issuperset(other)
   set >= other
   Test whether every element in other is in the set.
   
   set > other
   Test whether the set is a proper superset of other, that is, set >= other and set != other.
~~~~  
## Dictionary

*   A **dictionary** is a collection which is unordered, changeable and indexed. In Python dictionaries are written with curly brackets, and they have keys and values.
*   `keys()` ,`values()` , `items()` to loop through key / value in a dictionary
*   `fromkeys()` - Returns a dictionary with the specified keys and the specified value. `dict.fromkeys(keys, value)`
~~~~
    x = ('key1', 'key2', 'key3')
    y = 0
    thisdict = dict.fromkeys(x, y)
    o/p = ['key1': 0, 'key2': 0, 'key3': 0]      ## Default value is None in case Y is not there
~~~~
*   `setdefault()` - Returns the value of the item with the specified key.  If the key does not exist, insert the key, with the specified value.
~~~~
    car = {"brand": "Ford", "model": "Mustang", "year": 1964 }
    x = car.setdefault("color", "white")
    y = car.setdefault("model", "Bronco")
    print(x) # white
    print(y) # Mustang
~~~~

##  Key Difference
   * 
         
      |   | List   |  Tuple | Set  | Dictionary   |
      |---|-----|---|---|---|
      | **Format**| ["apple", "banana", "cherry"] |  ("apple", "banana", "cherry") | {"apple", "banana", "cherry"}  | `thisdict =  {"brand": "Ford", "model": "Mustang","year": 1964}` |
      |  **Construct** | list(("apple", "banana", "cherry")) | tuple(("apple", "banana", "cherry"))`  |   set(("apple", "banana", "cherry")) | dict(brand="Ford", model="Mustang", year=1964)  |
      | **Order**  | Ordered  |  Ordered	 |  **Unordered** |  **Unordered**  |
      | **Mutable**  | Yes   | No  	 |  No |Yes  | 
      | **Duplicate**  | Allowed |  Allowed	 | **Not Allowed**  |  **Not Allowed**  | 
      | **Indexed**  | Yes |  Yes	 | No |  Yes| 
      |**Access Item**|   |  	 |   | `thisdict["model"]` |
      |     |   |  	 |   | `thisdict.get("model")` |
      |  **Add Item**   | append() - `thislist.append("banana")` | Immutable  | add() - `thisset.add("orange")`  |  `thisdict["color"] = "red"` |
      |   | insert() - `thislist.insert(1, "orange")`  |   | update() - `thisset.update(["orange", "mango", "grapes"])`   |`thisdict.update({"color": "White"})`   |      
      | **Change Item**   |  Using index - `thislist[1] = "blackcurrant"` |  Immutable	 | Immutable  |Using Key - ` thisdict["year"] = 2018`  | 
      | **Remove Item**   |remove() - `thislist.remove("banana")`  |  Immutable	 |   `thisset.remove("banana")`  | | 
      |    |   |  Immutable	 | discard() - `thisset.discard("banana")`  | | 
      |   |  pop() - `thislist.pop(1)` | Immutable 	 | pop() - `thisset.pop()`  | `thisdict.pop("model")` | 
      |   |  |   |    | `thisdict.popitem()`  |
      | **Delete**   | `del thislist[0]` |  `del thistuple`	 |  `del thisset`  | ` del thisdict["model"]`| 
      |   | `thislist.clear()` |  Immutable	 |  `thisset.clear()` | `thisdict.clear()` | 
      | **Join**  | `list3 = list1 + list2`   |  `tuple3 = tuple1 + tuple2`	 | `set3 = set1.union(set2)`  |  | 
      |   |`list1.extend(list2)`   |  	 |  `set1.update(set2)` |  |             
      |   | ` list1.append(list2)` |  	 |   |  |    
      | **Copy**  |  `newlist= thislist.copy()`  |  `NA`	 | `newset= thisset.copy()`  | `newdict = thisdict.copy()` |    
      |   |   |  	 |   | `newdict = dict(thisdict)`|                      

## Exception Handling ( Try / Except)   

* `try` block lets you test a block of code for errors.

* `except` block lets you handle the error.  You can define as many exception blocks as you want, e.g. if you want to execute a special block of code for a special kind of error:

*  use the `else` keyword to define a block of code to be executed if no errors were raised:
* `finally` block lets you execute code, regardless of the result of the try- and except blocks.
~~~~
   try:
     f = open("demofile.txt")
     f.write("Lorum Ipsum")
   except NameError:
    print("Variable x is not defined")
   except:
     print("Something went wrong when writing to the file")
   else:
     print("Nothing went wrong")
   finally:
     f.close()
~~~~

* `raise` To throw (or raise) an exception.You can define what kind of error to raise, and the text to print .
~~~~
   x = "hello"
   if not type(x) is int:
   raise TypeError("Only integers are allowed")
~~~~
## Boolean

*  `bool()` function allows you to evaluate any value, and give you `True` or `False` in return.
  
   _Notes:-_
 
       Any string is True, except empty strings and None.
       Any number is True, except 0.
       Any list, tuple, set, and dictionary are True, except empty ones.
       If you have an object that is made from a class with a __len__ function that returns 0 or False

## Operator

*  **Identity Operators** - used to compare the objects, not if they are equal, but if they are actually the same object, with the same memory location
   
~~~~
   is 	Returns True if both variables are the same object	x is y	
   is not	Returns True if both variables are not the same object	x is not y
~~~~
*  **Membership Operators** - to test if a sequence is presented in an object
   
~~~~
   in 	Returns True if a sequence with the specified value is present in the object	x in y	
   not in	Returns True if a sequence with the specified value is not present in the object	x not in y
~~~~
## String Function
* Multiline Strings - Use three quotes. """ or '''
* String Slicing : Return a range of characters by specifying the `start index` 
   and the `end index(Not included)`, separated by a colon 
~~~~
  b = "Hello, World!"
  print(b[2:5] , b[-5:-2])
~~~~
* `translate()` -returns a string where some specified characters are replaced with the character described in a dictionary, 
   or in a mapping table `maketrans()`.

   _Note :-_ _If you use a dictionary, you must use ascii codes instead of characters._
                  
   `{x.translate({32: None}): y for x, y in student_list.items()}`
 
* `len()`  - Return length of the string `print(len("Hello, World!"))`
* `strip()` - removes any whitespace from the beginning or the end `print(" Hello, World! ".strip())`
* `lstrip()` - Returns a left trim version of the string
* `rstrip()` - Returns a right trim version of the string
* `lower()` & `casefold()` - Convert the string to lower case
* `upper()`  `print(a.upper())`
* `capitalize()` - Upper case the first letter in this sentence:
* `title()` - Returns a string where the first character in every word is upper case. Like a header, or a title.
   
   _Note :-_ If the word contains a number or a symbol, the first letter after that will be converted to upper case.
           

* `swapcase()` - Returns a string where all the upper case letters are lower case and vice versa.
* `replace()` -  Replaces a string with another string. `print("Hello, World!".replace("H", "J"))`
* `split()` - splits the string into substrings if it finds instances of the separator .`print("Hello, World!".split(","))`
* `rsplit()` - splits a string into a list, starting from the right.  If no "max" is specified, this method will return the same as the split() method.
   
   _Note:_ When maxsplit is specified, the list will contain the specified number of elements plus one.
  
     _Syntax_  `string.rsplit(separator, maxsplit)`

* `splitlines()` method splits a string into a list. The splitting is done at line breaks.

      _Syntax_    `string.splitlines(keeplinebreaks)`
 
     _Parameter Values_   `keeplinebreaks	Optional. Specifies if the line breaks should be included (True), or not (False). Default value is not (False)`

* `format()` - Formats specified values in a string .You can use index numbers {0} to change the order
~~~~
   quantity ,itemno , price = 3 , 567 , 49.95
   myorder = "I want {} pieces of item {} for {} dollars."  
             "I want to pay {2} dollars for {0} pieces of item {1}."
   print(myorder.format(quantity, itemno, price))
~~~~
* `count()` - returns the number of times a specified value appears in the string.
   
     _Syntax_  `string.count(value, start, end)`
 
* `center()` - Returns a centered string .  
  
     _Syntax_  `string.center(length, character)`
* `ljust()` - Return left align string, using a specified character (space is default) as the fill character. 

* `rjust()` - Returns a right justified version of the string

* `zfill()` - Adds zeros (0) at the beginning of the string, until it reaches the specified length.
   
    _Note:-_ If the value of the len parameter is less than the length of the string, no filling is done.
   
     _Syntax_  `string.zfill(len)`

* `endswith()` -  Returns `True` if the string ends with the specified value, otherwise `False`.
   
     _Syntax_    `string.endswith(value, start, end)`

* `startswith()` -  Returns `True` if the string ends with the specified value, otherwise `False`.

* `expandtabs()` method sets the tab size to the specified number of whitespaces. Def is **8**
   
     _Syntax_    `string.expandtabs(tabsize)`

* `find()` -  finds the first occurrence of the specified value. returns -1 if the value is not found.
   
     _Syntax_   `string.find(value, start, end)`

* `rfind()` - Return the last occurrence of the specified value . returns -1 if not found
* `index()` -  Same as `find()`. Only diff is it raises an exception if the value is not found.
* `rindex()` - Same as `rfind()`. Only diff is it raises an exception if the value is not found.
* `isalnum()` - Returns True if all the characters are alphanumeric. 

     _Syntax_  `string.isalnum()`

* `isalpha()` -  Returns True if all characters in the string are in the alphabet
* `isdecimal()` or `isdigit()` or `isnumeric()` - Returns True if all characters in the string are decimals
* `isidentifier()` - Returns `True` if the string is a valid identifier, otherwise `False`.
   
     _A string is considered a valid identifier if it only contains alphanumeric letters (a-z) and (0-9), 
   or underscores (\_). A valid identifier cannot start with a number, or contain any spaces._   

* `islower()` , `isupper()`
* `isprintable()` - Returns `True` if all the characters are printable. 
* `isspace()` -  Returns `True` if all the characters in a string are whitespaces
* `istitle()` - Returns `True` if all words in a text start with a upper case letter, AND the rest of the word are lower case letters, 
               otherwise False. _Symbols and numbers are ignored._

* `join()` - Takes all items in an iterable and joins them into one string. A string must be specified as the separator.
    
    _Note:_ When using a dictionary as an iterable, default returned values are the keys.
 
     _Syntax_   `string.join(iterable)`

*  `maketrans()` -  Returns a mapping table(Dictionary describing each repalcement) that can be used with the `translate()` method to replace specified characters.

     _Syntax_   `string.maketrans(x, y, z)`
~~~~ 
   **Parameter Values**
   x	Required. If only one parameter is specified, this has to be a dictionary describing how to perform the replace.
            If two or more parameters are specified, this parameter has to be a string specifying the characters you want to replace.
   y	Optional. A string with the same length as parameter x. Each character in the first parameter will be replaced with the corresponding character in this string.
   z	Optional. A string describing which characters to remove from the original string.

    **Example**
   txt = "Good night Sam!";
   x = "mSa";
   y = "eJo";
   z = "odnght";
   mytable = txt.maketrans(x, y, z);
   print(txt.translate(mytable));
   
   output:  G i Joe!
~~~~

*  `partition()` - Searches for a _first occurrence_ of specified string, and splits the string into a tuple containing three elements.
   _Note:-_ If the specified value is not found, the partition() method returns a tuple containing: 1 - the whole string, 2 - an empty string, 3 - an empty string:
~~~~
   1 - everything before the "match"
   2 - the "match"   
   3 - everything after the "match"
~~~~
    
    Syntax   `string.partition(value)`

* `rpartition()` -  Searches for a _Last occurrence_ of specified string
* Escape Character - backslash \ . To skip characters that are illegal in a string
~~~~
  txt = "We are the so-called \"Vikings\" from the north."
      = 'It\'s alright.'
  
  \n or \r => New line
  \t => Tab
  \b => Backspace
  \ooo => A backslash followed by three integers will result in a octal value:
          "\110\145\154\154\157"  => Hello
  \xhh => A backslash followed by an 'x' and a hex number represents a hex value:
          "\x48\x65\x6c\x6c\x6f"  => Hello
~~~~
# Order of execution in the Rock , Paper , Scissors Game
# r > s , s > p , p > r

import random


def play_the_game():
    user = input("Lets play the game , Please enter your options: 'r'(Rock) , 'p'(Paper) , 's'(Scissors) = ")
    computer = random.choice(['r', 'p', 's'])

    if user == computer:
        print('Game tied')

    if user != computer:
        is_win(user, computer)


def is_win(user, computer):
    if (user == 'r' and computer == 's') or (user == 's' and computer == 'p') or (user == 'p' and computer == 'r'):
        print('You won')
    else:
        print('You Lost')


play_the_game()

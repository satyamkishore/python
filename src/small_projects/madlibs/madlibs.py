'''Mad Libs is a phrasal template word game created by Leonard Stern and Roger Price. 
It consists of one player prompting others for a list of words to substitute for blanks in a story before reading aloud. 
The game is frequently played as a party game or as a pastime.'''

noun = input("Noun: ")
adj = input("Adjective: ")
verb = input("Verb: ")
another_verb = input("Verb: ")

madlib = f"Be kind to {noun}, this world include them as well. Some of them will be {adj} and some will not but \
    you have to make sure everyone {verb} with you if you want to {another_verb} far"

print(madlib)
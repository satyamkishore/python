import random

# def guess(limit):
#     number_to_guess = random.randint(1, limit)
#     guessed_by_user = 0

#     while guessed_by_user != number_to_guess:
#         guessed_by_user = int(input(f'Guess any one number between 1 and {limit} :'))
#         if guessed_by_user < number_to_guess:
#             print('Way low , Please guess again.')
#         elif guessed_by_user > number_to_guess:
#             print('Way high , Please guess again.')
    
#     print(f'Yay, You have guessed the number {number_to_guess} correctly')

# guess(10)

def guess_by_computer(max_limit):
    print(f"Hi There! Let's guess a number between 1 and {max_limit}")
    
    low = 1
    high = max_limit
    response_from_user = ''
    guessed_by_user = 0

    while response_from_user != 'C':
        guessed_by_user = random.randint(low, high)
        response_from_user = input(f"If your guessed number is {guessed_by_user} then press 'C' to complete the game or Press 'H'(High) or 'L'(Low) :")
        if response_from_user == 'H':
            high = guessed_by_user - 1
        elif response_from_user == 'L':
            low = guessed_by_user + 1
    
    print(f'Yay, Your guessed number is :: {guessed_by_user}')


guess_by_computer(10)
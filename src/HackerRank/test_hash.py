# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Problem~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Given an integer, n , and n space-separated integers as input, create a tuple, t , of those n integers.
# Then compute and print the result of hash(t).

if __name__ == '__main__':
    n = int(input('Total Number to enter:- '))
    integer_list = map(int, input('Items:- ').split())

    if n == 0 or n != len(list(integer_list)):
        print("Nothing to Hash or Items are not in range")
    else:
        tuple_list = tuple(integer_list)
        print(hash(tuple_list))

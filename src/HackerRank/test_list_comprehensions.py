# List comprehensions are an elegant way to build a list without having to use different for loops to
# append values one by one.


if __name__ == '__main__':
    x = int(input())
    y = int(input())
    z = int(input())
    n = int(input().strip())

# type A :    [ expression-involving-loop-variable for loop-variable in sequence ]
comprehension_list = [x for x in range(n)]
print(comprehension_list)

# type B : [ expression-involving-loop-variables for outer-loop-variable in outer-sequence for inner-loop-variable in inner-sequence ]
comprehension_list = [[x,y] for x in range(n) for y in [4, 5, 6]]
print(comprehension_list)

# type C : [ expression-involving-loop-variable for loop-variable in sequence if boolean-expression-involving-loop-variable ]
comprehension_list = [x for x in range(n) if x%2 == 0 ]
print(comprehension_list)

##################################Task############################
# You are given three integers x,y and z representing the dimensions of a cuboid along with an integer n .
# Print a list of all possible coordinates given by(i,j,k)  on a 3D grid where the sum of i+j+k is not equal to n
# 0<= i <= x and so on
comprehension_list = [[i,j,k] for i in range(x+1) for j in range(y+1) for k in range(z+1) if (i+j+k) != n ]
print(comprehension_list)
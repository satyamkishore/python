# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Problem~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# You are given a string and your task is to swap cases. 
# In other words, convert all lowercase letters to uppercase letters and vice versa.

# Example:

# Www.HackerRank.com → wWW.hACKERrANK.COM
# Pythonist 2 → pYTHONIST 2  
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def swap_case(s):
    return s.swapcase()
    # return "".join([ letter.lower() if letter == letter.upper() else letter.upper() for letter in s ])

if __name__ == '__main__':
    s = input('Input String:- ')
    result = swap_case(s)
    print(f'Swapped Case Result is:- {result}')
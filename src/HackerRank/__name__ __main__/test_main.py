# Every module in Python has a special attribute called __name__.
# The value of __name__ attribute is set to “__main__” when module is run as main program.
# Otherwise, the value of __name__ is set to contain the name of the module.
# We use if __name__ == “__main__” block to prevent (certain) code from being run when the module is imported.


import test_steps

print("Name here is : ", __name__)
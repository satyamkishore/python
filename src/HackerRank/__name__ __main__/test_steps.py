def a_function():
    print("__name__ & __main__ called by same program")

if __name__ =='__main__':
    a_function()
else:
    print("__main__ & __name__ called by import")

print('__name__ is: ',__name__)
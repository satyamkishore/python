# Task
# The provided code stub reads two integers from STDIN, a and b. Add code to print three lines where:
#
# The first line contains the sum of the two numbers.
# The second line contains the difference of the two numbers (first - second).
# The third line contains the product of the two numbers.
# The first line should contain the result of integer division, a//b . The second line should contain the result of float division,  a/b .



a , b = int(input()), int(input())

print('%d\n%d\n%d\n%d\n%f'%((a+b),(a-b),(a*b),(a//b),(a/b)))
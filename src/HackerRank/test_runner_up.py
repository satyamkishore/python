# Given the participants score sheet for your University Sports Day, you are required to find the runner-up score.
# You are given n scores. Store them in a list and find the score of the runner-up.

if __name__ == '__main__':
    n = int(input('Enter number of score: ').strip())
    score_ls = list(map(int, input('Enter individual Scores: ').strip().split()))[:n] # split() to convert a string into a list.
    # score_ls = [int(x) for x in input().strip().split()]
    runner_score_ls = [x for x in score_ls if x < max(score_ls)]
    print(max(runner_score_ls))
#
# Given the names and grades for each student in a class of N students,
# store them in a nested list and print the name(s) of any student(s) having the second lowest grade.
#
# Note: If there are multiple students with the second lowest grade, order their names alphabetically and print each name on a new line.

if __name__ == '__main__':
    students = []
    for _ in range(int(input())):
        name = input()
        score = float(input())
        students.append([name,score])
    # students = [['Harry', 26.9], ['Zerry', 37.21], ['Tina', 37.21], ['Akriti', 41], ['Harsh', 39]]
    min_score = min(students , key=lambda y: y[1])
    min_sec_score = min([x for x in students if x[1] > min_score[1]], key=lambda x: x[1])
    min_sec_list = [x for x in [x for x in students if x[1] > min_score[1]] if x[1] == min_sec_score[1]]
    for _ in sorted(min_sec_list):
        print(_[0])
from random import randrange
from collections import Counter
# https://pymotw.com/2/collections/counter.html

# Initializing
print(Counter(['a','b','c','d','c','a']))
print(Counter({'a':2, 'c':2, 'b':1, 'd':1}))
print(Counter(a=2 , b= 3, c=1))

count_var = Counter()
print('Initial Counter: ', count_var)
count_var.update('abbcddddaac')
print('Updated Counter: ', count_var)

count_var.update({'a':3, 'b':1})
print('Another Update: ', count_var)

count_var.update(c=2)

print('one moe update test: ', count_var)

# Accessing Counts

count_var = Counter('abbcddddaac')

for letter in 'abcde':
    print('%s : %d' % (letter , count_var[letter]))

count_var = Counter('extremely')
count_var['z'] = 0
print(count_var,'\n',list(count_var.elements()))

sample_string = input().strip()
count_var =Counter(sample_string)
print(dict(count_var))


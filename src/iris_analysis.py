import os
import sys
from pandas import read_csv
from pandas.plotting import scatter_matrix
from matplotlib import pyplot
from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC

#print(os.path.dirname(sys.modules['__main__'].__file__))

# load dataset
file_name=os.path.join(sys.path[1],"reference_file" ,"iris.csv").replace("\\" ,"/")
header = ['sepal-length', 'sepal-width', 'petal-length', 'petal-width', 'class']
dataset = read_csv(file_name , names=header)

# # shape
# print(dataset.shape)
#
# # head
# print(dataset.head(10))
#
# # description
# print(dataset.describe())
#
# # Group by class distribution
# print(dataset.groupby('class').size())
#
# # Uni-variate Plots
# # Box and whisker plot
dataset.plot(kind='box' , subplots = True , layout=(2,2))
# # Histogram
dataset.hist()
#
# # Multi-variate Plots
# # Scatter Plot Matrix 
scatter_matrix(dataset)
pyplot.show()

print("log")


# SPlit out validation dataset
array = dataset.values
X = array[:, 0:4]
Y = array[:,4]
X_train, X_validation, Y_train, Y_validation = train_test_split(X, Y, train_size=.70, test_size=0.30, random_state=1)

# Build Models
models = []
models.append(('LR',LogisticRegression(solver='liblinear', multi_class='ovr')))
models.append(('LDA' , LinearDiscriminantAnalysis()))
models.append(('KNN' , KNeighborsClassifier()))
models.append(('CART' , DecisionTreeClassifier()))
models.append(('NB' , GaussianNB()))
models.append(('SVM' , SVC(gamma='auto')))
# Evaluate each model in turn
results = []
names = []

kfold = StratifiedKFold(n_splits=10,random_state=1 , shuffle=True)
# for train , test  in kfold.split(X,Y):
#     print('Train: %d Test: %d'%(train.size,test.size))

for name, model in models:
    cv_results = cross_val_score(model , X_train , Y_train , cv=kfold , scoring='accuracy')
    results.append(cv_results)
    names.append(name)
    print('%s: %f (%f)' % (name, cv_results.mean() , cv_results.std()))

# Compare algorithm
pyplot.boxplot(results, labels=names)
pyplot.title('Algorithm Comparison')
pyplot.show()

# make prediction
model = SVC(gamma='auto')
model.fit(X_train,Y_train)
prediction = model.predict(X_validation)

# evaluate predictions
print(accuracy_score(Y_validation,prediction))
print(confusion_matrix(Y_validation,prediction))
print(classification_report(Y_validation,prediction))